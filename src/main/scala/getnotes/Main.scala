package getnotes

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration

import argonaut.JsonIdentity
import getnotes.pbmodels.{TagLocation, TagReader}
import getnotes.pbtables.TagsProvider
import org.rogach.scallop.{ScallopConf, Subcommand, ValueConverter}
import slick.jdbc.SQLiteProfile
import slick.jdbc.SQLiteProfile.api._
import kantan.csv._
import kantan.csv.ops._
import getnotes.pbmodels.HighlightLocation
import getnotes.pbmodels.NoteLocation
import getnotes.pbmodels.BookmarkLocation
import getnotes.pbtables.Book
import java.time.ZonedDateTime
import java.time.Clock
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.time.ZoneId

sealed trait OutputFormat
object Text extends OutputFormat
object Csv extends OutputFormat
object Json extends OutputFormat
object PrettyText extends OutputFormat
object SExp extends OutputFormat

object OutputFormat {
  implicit val valueConverter = new ValueConverter[OutputFormat] {
    val argType = org.rogach.scallop.ArgType.SINGLE
    def parse(
        s: List[(String, List[String])]
    ): Either[String, Option[OutputFormat]] =
      s match {
        case (_, "Text" :: Nil) :: Nil   => Right(Some(Text))
        case (_, "Csv" :: Nil) :: Nil    => Right(Some(Csv))
        case (_, "Json" :: Nil) :: Nil   => Right(Some(Json))
        case (_, "Pretty" :: Nil) :: Nil => Right(Some(PrettyText))
        case (_, "Sexp" :: Nil) :: Nil   => Right(Some(SExp))
        case Nil                         => Right(None)
        case _                           => Left("Output format must be one of Text, Csv, Json, Pretty")
      }
  }
}

private class BooksSubcommand extends Subcommand("books") {
  val titlePart = opt[String](name = "title-part", short = 't')
}

private class NotesSubcommand extends Subcommand("notes") {
  val bookId = opt[Int](name = "book-id", short = 'b', required = true)
}

private class Options(args: Seq[String]) extends ScallopConf(args) {

  val books = new BooksSubcommand()
  addSubcommand(books)

  val notes = new NotesSubcommand()
  addSubcommand(notes)

  val dbPath = opt[String](name = "db-path", short = 'd')
  val outputFormat =
    opt[OutputFormat](name = "output-format", short = 'o', default = Some(Text))

  verify()
}

object Main {

  def prettyPrint(tl: TagLocation): String = tl match {
    case HighlightLocation(page, text, date) => s"""strona ${page}, ${text}"""
    case NoteLocation(page, text, date, note) =>
      s"""strona ${page}, "${text}": ${note}"""
    case BookmarkLocation(page) => s""" UNIMPLEMENTED """
  }

  val DEFAULT_DB_PATH = "/media/PB631/system/config/books.db"
  val DB_ENV_VAR = "PB_BOOKS_DB"

  def main(args: Array[String]): Unit = {
    val options = new Options(args)

    val dbPath = options.dbPath.toOption
      .orElse(sys.env.get(DB_ENV_VAR))
      .getOrElse(DEFAULT_DB_PATH)

    val db = Database.forURL(s"jdbc:sqlite:$dbPath")
    val tp = new TagsProvider(db)

    options.subcommands.foreach { subcommand =>
      subcommand match {
        case bc: BooksSubcommand => {
          val query =
            bc.titlePart.toOption.map(tp.findBooks _).getOrElse(tp.listAllBooks)
          val books = Await.result(query, Duration.Inf)
          options.outputFormat() match {
            case Text => books.foreach(println)
            case Csv  => Console.out.writeCsv(books, rfc.withHeader)
            case Json =>
              println(JsonIdentity.ToJsonIdentity(books.toList).asJson)
            case PrettyText =>
              books.foreach {
                case Book(oid, title, author, timeAdded) => {
                  val aut = author.map(_ + ". ").getOrElse("")
                  val time = DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(
                    ZonedDateTime.ofInstant(
                      Instant.ofEpochSecond(timeAdded),
                      ZoneId.of("UCT")
                    )
                  )
                  println(s"${oid}: ${aut}${title}, added ${time}")
                }
              }
            case SExp => {
              print("(")
              books.foreach{case Book(oid, title, author, timeAdded) => {
                //FIXME string escape
                val auth = author.map("author \""+_+"\"").getOrElse("")
                print(s"""(oid ${oid} title "${title}" timeAdded ${timeAdded} ${auth}) """);
              }}
              print(")")
            }
          }
        }
        case nc: NotesSubcommand => {
          val notes: Either[List[Option[String]], List[TagLocation]] = Await
            .result(tp.tagsForBook(nc.bookId()), Duration.Inf)
            .map(x => TagReader.readTags(x._2.toList))
            .foldRight[Either[List[Option[String]], List[TagLocation]]](
              Right(List())
            )((el, l) => l.flatMap(l => el.map(_ :: l)))
          notes match {
            case Right(nn) =>
              options.outputFormat() match {
                case Text => nn.sortBy(_.page).foreach(x => println(x))
                case Json => {
                  import pbmodels.TagLocation._
                  println(
                    JsonIdentity.ToJsonIdentity(nn.toList.sortBy(_.page)).asJson
                  )
                }
                case Csv =>
                  Console.out.writeCsv(nn.sortBy(_.page), rfc.withHeader)
                case PrettyText => {
                  nn.sortBy(_.page)
                    .foreach(x => println(s" - ${prettyPrint(x)}"))
                }
                case SExp      => {
                  print("(")
                  //FIXME string escape
                  nn.foreach{
                    case BookmarkLocation(page) => print(s"(type bookmark pate ${page}) ")
                    case HighlightLocation(page, text, date) => print("""(type highlight page ${page} text "${text}" date ${date}) """)
                    case NoteLocation(page, text, date, note) => print("""(type note page ${page} text "${text}" date ${date} node "${node}") """)
                  }
                  print(")")
                }
              }
            case Left(err) => Console.err.println(err)
          }
        }
      }
    }
  }
}
