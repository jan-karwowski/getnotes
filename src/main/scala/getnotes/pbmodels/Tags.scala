package getnotes.pbmodels

import argonaut.{ DecodeJson, DecodeResult, Parse }
import scalaz.NonEmptyList
import scalaz.syntax.ValidationOps
import scalaz.{ Failure, Success, Validation }

sealed trait Tags 

sealed trait Types extends Tags

object Note extends Types
object Highlight extends Types
object Bookmark extends Types


object Types {
  val decode: String => Validation[String, Types] = x =>
    x match {
      case "note" => Success(Note)
      case "highlight" => Success(Highlight)
      case "{}" => Success(Highlight)
      case "yellow" => Success(Highlight)
      case "bookmark" => Success(Bookmark)
      case _ => Failure(s"unknown type ${x}")
    }
}


final case class Location(
  anchor: String,
  created: Int
) extends Tags

object Location {
  implicit val decodeJson: DecodeJson[Location] = DecodeJson.jdecode2L(Location.apply)("anchor", "created")
}


final case class NoteText(
  text: String
) extends Tags

object NoteText {
  implicit val decodeJson: DecodeJson[NoteText] = DecodeJson.jdecode1L(NoteText.apply)("text")
}


final case class HighlightText(
  begin: String,
  end: String,
  text: String
) extends Tags

object HighlightText {
  implicit val decodeJson: DecodeJson[HighlightText] = DecodeJson.jdecode3L(HighlightText.apply)("begin", "end", "text")
}

object Tags {
  implicit val decodeJson : DecodeJson[Tags] =
    HighlightText.decodeJson |||[Location, Tags] Location.decodeJson ||| NoteText.decodeJson

  val decodeJsons : String => Validation[String, Tags] =
    x => Validation.fromEither(Parse.decode(x).left.map(_.toString))

  def decode(x: String) : Validation[NonEmptyList[String], Tags] = Types.decode(x).toValidationNel findSuccess decodeJsons(x).toValidationNel
}
