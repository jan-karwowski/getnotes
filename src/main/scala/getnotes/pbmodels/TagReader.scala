package getnotes.pbmodels

import java.time.{ Instant, LocalDateTime, ZoneId, ZonedDateTime }
import java.util.Calendar
import scala.util.Try
import scala.util.matching.Regex
import scalaz.{ Failure, NonEmptyList, Success, Validation }


object TagReader {

  final case class HighlightLocationBuilder(
    page: Option[Int],
    text: Option[String],
    date: Option[ZonedDateTime]
  ) {
    def create : Either[List[Option[String]], HighlightLocation] =
      Try(HighlightLocation(page.get, text.get, date.get)).toEither.left.map(x => List(Some(x.toString)))
  }

  final case class NoteLocationBuilder(
    page: Option[Int],
    text: Option[String],
    date: Option[ZonedDateTime],
    note: Option[String]
  ) {
    def create : Either[List[Option[String]], NoteLocation] =
      Try(NoteLocation(page.get, text.get, date.get, note.get)).toEither.left.map(x => List(Some(x.toString)))
  }

  // ?page=2&
  def pageFromAnchor(str: String) : Int = {
    val regex =new Regex("\\?page=([0-9]*)\\&", "num")
    regex.findFirstMatchIn(str).get.group("num").toInt+1
  }

  def highlightUpdate(hlb: HighlightLocationBuilder, tags: Tags) = {
    tags match {
      case Location(anchor, created) => hlb.copy(page = Some(pageFromAnchor(anchor)),
        date = Some(ZonedDateTime.ofInstant(Instant.ofEpochSecond(created), ZoneId.of("UTC"))))
      case HighlightText(begin, end, text) => hlb.copy(text = Some(text))
      case _ => hlb
    }
  }

  def noteUpdate(hlb: NoteLocationBuilder, tags: Tags) = {
    tags match {
      case Location(anchor, created) => hlb.copy(page = Some(pageFromAnchor(anchor)),
        date = Some(ZonedDateTime.ofInstant(Instant.ofEpochSecond(created), ZoneId.of("UTC"))))
      case HighlightText(begin, end, text) => hlb.copy(text = Some(text))
      case NoteText(text) => hlb.copy(note = Some(text))
      case _ => hlb
    }
  }


  def readTags(str: List[String]) : Either[List[Option[String]], TagLocation] = {
    val decoded: List[Validation[NonEmptyList[String], Tags]] = str.map(Tags.decode)
    val good = decoded.foldLeft(Option(List[Tags]()))((ol, el) => el match {
      case Success(x) => ol.map(l => x::l)
      case Failure(_) => None
    })

    good match {
      case Some(list) => {
        list.collect{case (x: Types) => x}  match {
          case List(Highlight) => list.foldLeft(HighlightLocationBuilder(None, None, None))(highlightUpdate).create
          case List(Highlight, Highlight) => list.foldLeft(HighlightLocationBuilder(None, None, None))(highlightUpdate).create
          case List(Highlight, Highlight, Highlight) => list.foldLeft(HighlightLocationBuilder(None, None, None))(highlightUpdate).create
          case List(Note) =>  list.foldLeft(NoteLocationBuilder(None, None, None, None))(noteUpdate).create
          case List(_, Note) =>  list.foldLeft(NoteLocationBuilder(None, None, None, None))(noteUpdate).create
          case List(Bookmark) => Right(BookmarkLocation(-1))
          case ts => Left(List(Some(s"bad type sepcifier in list ${ts}")))
        }
      }
      case None => Left(decoded.map(_.swap.map((Some.apply[String] _).compose((_ : NonEmptyList[String]).toString)).getOrElse(None)))
    }
  }
}
