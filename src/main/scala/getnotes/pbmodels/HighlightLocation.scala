package getnotes.pbmodels

import argonaut.EncodeJson
import java.time.ZonedDateTime
import kantan.csv.{ HeaderEncoder, RowEncoder }
import kantan.csv.java8._

sealed trait TagLocation {
  val page: Int
}

final case class HighlightLocation(
  page: Int,
  text: String,
  date: ZonedDateTime
) extends TagLocation

final case class NoteLocation(
  page: Int,
  text: String,
  date: ZonedDateTime,
  note: String
) extends TagLocation

final case class BookmarkLocation(
  page: Int
) extends TagLocation

object TagLocation {
  implicit val dateEncode : EncodeJson[ZonedDateTime] = EncodeJson.StringEncodeJson.contramap(
    _.toString
  )

  implicit val csvEncode : HeaderEncoder[TagLocation] = HeaderEncoder.encoder("page", "text", "date", "note"
  )((tl: TagLocation) => tl match {
    case HighlightLocation(page, text, date) => (page, text, date, "")
    case NoteLocation(page, text, date, note) => (page, text, date, note)
    case BookmarkLocation(page) => (page, "", ZonedDateTime.now(), "")
  })

  val hle = EncodeJson.jencode3L((HighlightLocation.unapply _) andThen (_.get)
    )("page", "text", "date")
  val nle = EncodeJson.jencode4L((NoteLocation.unapply _) andThen (_.get)
  )("page", "text", "date", "note")
  val ble = EncodeJson.jencode1L((BookmarkLocation.unapply _) andThen (_.get))("page")

  implicit val jsonEncode : EncodeJson[TagLocation] = EncodeJson(tl => tl match {
    case hl : HighlightLocation => hle(hl)
    case nl : NoteLocation => nle(nl)
    case bl : BookmarkLocation => ble(bl)
  })
}
