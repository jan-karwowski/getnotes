package getnotes.pbtables

import slick.jdbc.SQLiteProfile.api._
import slick.lifted.{Tag => STag}

final case class Tag(
  oid: Int,
  itemId: Int,
  tagId: Int,
  value: String,
  timeEdt: Int
)

final class Tags(tag: STag) extends Table[Tag](tag, "Tags") {
  def oid = column[Int]("oid", O.PrimaryKey)
  def itemId = column[Int]("itemId")
  def tagId = column[Int]("tagId")
  def value = column[String]("Val")
  def timeEdt = column[Int]("TimeEdt")

  def * = (oid, itemId, tagId, value, timeEdt) <> ((Tag.apply _).tupled, Tag.unapply)

  def item = foreignKey("itemId", itemId, Items.query)(_.oid)
}

object Tags {
  lazy val query = TableQuery[Tags]
}
