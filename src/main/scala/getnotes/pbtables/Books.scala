package getnotes.pbtables

import slick.jdbc.SQLiteProfile.api._
import slick.lifted.{Tag => STag}
import argonaut.CodecJson
import kantan.csv.HeaderEncoder

final case class Book(
  oid: Int,
  title: String,
  authors: Option[String],
  timeAdd: Int 
)

object Book {
  implicit val codec = CodecJson.casecodec4(Book.apply, Book.unapply)(
    "oid", "title", "authors", "timeAdded"
  )
  implicit val headerEncoder = HeaderEncoder.caseEncoder("oid", "title", "authors", "timeAdded")(Book.unapply)
}

final class Books(tag: STag) extends Table[Book](tag, "Books") {
  def oid = column[Int]("OID", O.PrimaryKey)
  def title = column[String]("Title")
  def authors = column[Option[String]]("Authors")
  def timeAdd = column[Int]("TimeAdd")

  def * = (oid, title, authors, timeAdd) <> ((Book.apply _).tupled, Book.unapply)

  def item = foreignKey("oid", oid, Items.query)(_.oid)
}

object Books {
  lazy val query = TableQuery[Books]
}
