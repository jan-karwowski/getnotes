package getnotes.pbtables

import scala.concurrent.{ Await, ExecutionContext, Future }
import slick.jdbc.SQLiteProfile.api._
import slick.lifted.{Tag => STag}

final class TagsProvider(db: Database) {
  def listAllBooks : Future[Seq[Book]] = db.run(Books.query.result)

  def findBooks(namePart: String) : Future[Seq[Book]] =
    db.run(Books.query.filter(_.title.like(s"%${namePart}%")).result)

  def tagsForBook(bookId: Int)(
    implicit ec: ExecutionContext
  ) : Future[Seq[(Int, Seq[String])]] = db.run(
    Tags.query.join(Items.query).on((t, i) => t.itemId === i.oid).join(Items.query)
      .on{case ((t, i), p) => p.oid === i.parent}
      .filter{case ((t, i), p) => i.parent === bookId && i.state === 0}.map{case ((t, i), p) =>
        (t.itemId, t.value, i.state, p.state)}.result
  ).map(_.groupBy(_._1).mapValues(_.map(x => x._2)).toSeq)
}
