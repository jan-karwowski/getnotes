package getnotes.pbtables

import slick.jdbc.SQLiteProfile.api._
import slick.lifted.{Tag => STag}

final case class File(
  oid: Int,
  bookId: Int,
  pathId: Int,
  name: String,
  len: Int,
  ord: Int
)

final class Files(tag: STag) extends Table[File](tag, "Files") {
  def oid = column[Int]("oid", O.PrimaryKey)
  def name = column[String]("name")
  def bookId = column[Int]("bookId")
  def pathId = column[Int]("pathId")
  def len = column[Int]("len")
  def org = column[Int]("ord")

  def * = (oid, bookId, pathId, name, len, org) <> ((File.apply _).tupled, File.unapply)

  def book = foreignKey("bookId", bookId, Books.query)(_.oid)
}

object Files {
  lazy val query = TableQuery[Files]
}
