package getnotes.pbtables

import slick.jdbc.SQLiteProfile.api._
import slick.lifted.{Tag => STag}

final case class Item(
  oid: Int,
  typeId: Int,
  parent: Option[Int], 
  state: Int,
  timeAlt: Int,
  hashUuid: String
)

final class Items(tag: STag) extends Table[Item](tag, "Items") {
  def oid = column[Int]("oid", O.PrimaryKey)
  def typeId = column[Int]("typeId")
  def parent = column[Option[Int]]("parentId")
  def state = column[Int]("state")
  def timeAlt = column[Int]("timeAlt")
  def hashUuid = column[String]("hashUuid")

  def * = (oid, typeId, parent, state, timeAlt, hashUuid) <> ((Item.apply _).tupled, Item.unapply)

  def parentItem = foreignKey("parent", parent, Items.query)(_.oid)
}

object Items {
  lazy val query = TableQuery[Items]
}
