enablePlugins(PackPlugin)

name := "getnote"
version := "0.0-SNAPSHOT"
organization := "jk"
scalaVersion := "2.12.10"
scalaVersion in ThisBuild := "2.12.10"
scalacOptions := Seq("-unchecked", "-feature", "-deprecation")
javacOptions ++= Seq("-parameters")
fork := true


libraryDependencies ++= Seq(
  "org.rogach" %% "scallop" % "3.1.5",
  "org.slf4s" %% "slf4s-api" % "1.7.25",
  "ch.qos.logback" % "logback-classic" % "1.1.2",
  "com.typesafe.slick" %% "slick" % "3.2.3",
  "org.xerial" % "sqlite-jdbc" % "3.8.6",
  "io.argonaut" %% "argonaut" % "6.2.2" ,
  "org.scalaz" %% "scalaz-core" % "7.2.27",
  "com.nrinaudo" %% "kantan.csv" % "0.5.0",
  "com.nrinaudo" %% "kantan.csv-java8" % "0.5.0",
  "com.nrinaudo" %% "kantan.csv-generic" % "0.5.0",
  "org.scalatest" %% "scalatest" % "3.0.1" % Test,
  "org.scalamock" %% "scalamock-scalatest-support" % "3.5.0" % Test
)
