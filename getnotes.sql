SELECT group_concat(t.val) FROM tags as t inner join items as i1 on i1.OID=t.ItemID
  inner join items as i2 on i1.ParentID=i2.OID
  inner join books as b on i2.OID=b.OID
  inner join files as f on f.BookID=b.OID
  where i1.state=0 AND f.name='pz-lab-2-projekt-systemu.pdf' AND t.Val <> 'note' AND t.Val <> 'highlight' group by t.ItemID;
