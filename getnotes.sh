#!/bin/sh

sqlite3 /media/PB631/system/config/books.db < getnotes.sql | \
    sed -e '1i [' -e '$a ]' -e '1s/^/[/' -e '2,$s/^/,[/' -e 's/$/]/' |\
     jq '.[]|{"page":nth(1).begin, "text": nth(1).text, "comment": nth(2).text}'
